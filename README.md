# Twilio SDK Starter Application for C#

## .NET Core 2.1.x Project

This sample project demonstrates how to use Twilio Chat in a C# web
application. 

## Setup

1. Install [.NET Core](https://www.microsoft.com/net/core).
2. Clone this repository:
    ```bash
    git clone https://slava_lukash@bitbucket.org/slava_lukash/twiliochat.git
    ```

## Configure the sample application

To run the application, you'll need to gather your Twilio account credentials and configure them
in User Secrets via the `secrets.json` file. If you are unsure how to do this, check out this blog post on [User Secrets](https://www.twilio.com/blog/2018/05/user-secrets-in-a-net-core-web-app.html).
These credentials should mirror those in the `appsettings.json` file found in the root of the `TwilioChat.Web` project.

### Configure account information

Every sample in the demo requires some basic credentials from your Twilio account. Configure these first.

| Config Value       | Description                                                                                                           |
| :----------------- | :-------------------------------------------------------------------------------------------------------------------- |
| `TwilioAccount:AccountSid` | Your primary Twilio account identifier - find this [in the console here](https://www.twilio.com/console).             |
| `TwilioAccount:ApiKey`     | Used to authenticate - [generate one here](https://www.twilio.com/console/dev-tools/api-keys).                        |
| `TwilioAccount:ApiSecret`  | Used to authenticate - [just like the above, you'll get one here](https://www.twilio.com/console/dev-tools/api-keys). |

To set a configuration value, use the `dotnet` command line:

```bash
cd /TwilioChat.Web
dotnet user-secrets set "TwilioAccount:AccountSid" "ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
dotnet user-secrets set "TwilioAccount:ApiKey" "SKXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
dotnet user-secrets set "TwilioAccount:ApiSecret" "xxxxxxxxxxxxxxxxxxxxxxxx"
```

#### A Note on API Keys

When you generate an API key pair at the URLs above, your API Secret will only be shown once -
make sure to save this information in a secure location.

## Configure product-specific settings

Depending on which demos you'd like to run, you may need to configure a few more values.

### Configuring Twilio Chat

In addition to the above, you'll need to [generate a Chat Service](https://www.twilio.com/console/chat/services) in the Twilio Console. Put the result in your secrets.

| Config Value           | Where to get one.                                                                       |
| :--------------------- | :-------------------------------------------------------------------------------------- |
| `TwilioAccount:ChatServiceSid` | [Generate one in the Twilio Chat console](https://www.twilio.com/console/chat/services) |

```bash
cd /TwilioChat.Web
dotnet user-secrets set "TwilioAccount:ChatServiceSid" "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```


## Run The Application

### Visual Studio

Open `TwilioChat.sln` and press _F5_ or click the Run button

### Windows CLI, OS X or Linux

```bash
cd TwilioChat.Web
dotnet restore
dotnet run
```

Your application should now be running at [https://localhost:44387/](https://localhost:44387/).

## License

MIT
