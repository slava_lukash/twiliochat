﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Twilio;
using Twilio.Jwt.AccessToken;
using TwilioChat.Web.Models;

namespace TwilioChat.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilitiesController : ControllerBase
    {
        private readonly TwilioAccount _twilioAccount;

        public UtilitiesController(IOptions<TwilioAccount> twilioAccount)
        {
            if (twilioAccount == null)
            {
                throw new ArgumentNullException(nameof(twilioAccount));
            }
            _twilioAccount = twilioAccount.Value;

            TwilioClient.Init(
                    _twilioAccount.ApiKey,
                    _twilioAccount.ApiSecret,
                    _twilioAccount.AccountSid
                );
        }

        [HttpGet("/token")]
        public JsonResult Token(string identity, string channel)
        {            
            //string identity;
            //Request.Cookies.TryGetValue("UserName", out identity);

            //string channel;
            //Request.Cookies.TryGetValue("ChannelSid", out channel);

            var grants = new HashSet<IGrant>();
            if (_twilioAccount.ChatServiceSid != String.Empty)
            {
                // Create a "grant" which enables a client to use IPM as a given user,
                // on a given device.
                var chatGrant = new ChatGrant()
                {
                    ServiceSid = _twilioAccount.ChatServiceSid
                };

                grants.Add(chatGrant);
            }

            var token = new Token(

                _twilioAccount.AccountSid,
                _twilioAccount.ApiKey,
                _twilioAccount.ApiSecret,
                identity,
                grants: grants
            ).ToJwt();

            return new JsonResult(new Dictionary<string, string>()
            {
                {"identity", identity},
                {"channel", channel},
                {"token", token}
            });
        }
    }
}