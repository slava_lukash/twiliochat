﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using Twilio;
using Twilio.Rest.Chat.V1.Service;
using TwilioChat.Web.Models;
using Twilio.Rest.Chat.V1.Service.Channel;
using static Twilio.Rest.Chat.V1.Service.ChannelResource;
using Twilio.Rest.Chat.V1.Service.User;
using System.Collections.Generic;

namespace TwilioChat.Web.Controllers
{
    public class ChatController : Controller
    {
        private readonly TwilioAccount _twilioAccount;
        private readonly ILogger _logger;

        public ChatController(IOptions<TwilioAccount> twilioAccount, ILoggerFactory logger)
        {
            _logger = logger.CreateLogger("TwilioChat");
            _twilioAccount = twilioAccount.Value ?? throw new ArgumentException(nameof(twilioAccount));
            TwilioClient.Init(_twilioAccount.ApiKey, _twilioAccount.ApiSecret, _twilioAccount.AccountSid);
        }

        public IActionResult Index(string userName, string channelSid)
        {
            //string userName = string.Empty;
            //Request.Cookies.TryGetValue("UserName", out userName);

            // Get list all users.
            var users = UserResource.Read(pathServiceSid: _twilioAccount.ChatServiceSid, limit: 20);

            // Get a user.
            UserResource chatUser = null;
            foreach (var user in users)
            {
                if (user.Identity == userName)
                {
                    chatUser = user;
                    break;
                }
            }

            // Create a user.
            if (chatUser == null)
            {
                chatUser = UserResource.Create(
                    identity: userName,
                    pathServiceSid: _twilioAccount.ChatServiceSid
                );
            }

            // Get the user's channels.
            var channels = new List<ChannelModel>();

            var userChannels = UserChannelResource.Read(
                pathServiceSid: _twilioAccount.ChatServiceSid,
                pathUserSid: chatUser.Sid
            );

            foreach (var record in userChannels)
            {
                // Find the member in the channels.
                var channelMembers = MemberResource.Read(
                    pathServiceSid: _twilioAccount.ChatServiceSid,
                    pathChannelSid: record.ChannelSid
                );

                channels.Add(new ChannelModel()
                {
                    ChannelSid = record.ChannelSid,
                    Member = channelMembers.Where(x => x.Identity != chatUser.Identity).Select(x => x.Identity).First(),
                    UnreadMessagesCount = record.UnreadMessagesCount
                });
            }

            // Set default channel.
            if (string.IsNullOrEmpty(channelSid) || !channels.Any(x => x.ChannelSid == channelSid))
            {
                channelSid = channels.Select(x => x.ChannelSid).FirstOrDefault();
            }

            // Get last messages.
            var messages = new List<MessageResource>();

            try
            {
                messages = MessageResource.Read(
                   pathServiceSid: _twilioAccount.ChatServiceSid,
                   pathChannelSid: channelSid,
                   limit: 20
               ).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            var model = new ChatModel()
            {
                UserName = userName,
                ChannelSid = channelSid,
                Users = users.Where(x => x.Sid != chatUser.Sid).ToList(),
                Channels = channels,
                Messages = messages

            };

            // Response.Cookies.Append("ChannelSid", channelSid);
            return View(model);
        }

        // Add a new channel.
        [HttpPost]
        public void Create(string userName, string member)
        {
            // Create a channel.
            var channel = ChannelResource.Create(
                pathServiceSid: _twilioAccount.ChatServiceSid,
                uniqueName: $"{userName}_{member}",
                type: ChannelTypeEnum.Private
            );

            // Add members.
            var member1 = MemberResource.Create(
                identity: userName,
                pathServiceSid: _twilioAccount.ChatServiceSid,
                pathChannelSid: channel.Sid
            );
            var member2 = MemberResource.Create(
                identity: member,
                pathServiceSid: _twilioAccount.ChatServiceSid,
                pathChannelSid: channel.Sid
            );
        }

        // Delete the channel.
        [HttpGet]
        public IActionResult Delete(string userName, string channelSid)
        {
            ChannelResource.Delete(
                pathServiceSid: _twilioAccount.ChatServiceSid,
                pathSid: channelSid
            );

            return RedirectToAction("Index", new { userName, channelSid });
        }
    }
}