﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using Twilio;
using Twilio.Rest.Chat.V1.Service;
using TwilioChat.Web.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TwilioChat.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly TwilioAccount _twilioAccount;

        public HomeController(IOptions<TwilioAccount> twilioAccount)
        {
            _twilioAccount = twilioAccount.Value ?? throw new ArgumentException(nameof(twilioAccount));
            TwilioClient.Init(_twilioAccount.ApiKey, _twilioAccount.ApiSecret, _twilioAccount.AccountSid);
        }

        public IActionResult Index()
        {
            // Get list all users.
            var users = UserResource.Read(pathServiceSid: _twilioAccount.ChatServiceSid);
            var model = new LoginModel()
            {
                Users = new SelectList(users.Select(x => x.Identity).ToList(), "Identity")
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Login(string userName)
        {
            // Response.Cookies.Append("UserName", userName);
            return RedirectToAction("Index", "Chat", new { userName });
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
    }
}
