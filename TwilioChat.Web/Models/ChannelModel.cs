﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwilioChat.Web.Models
{
    public class ChannelModel
    {
        public string ChannelSid;
        public string Member;
        public int? UnreadMessagesCount;
    }
}
