﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio.Rest.Chat.V1.Service;
using Twilio.Rest.Chat.V1.Service.Channel;

namespace TwilioChat.Web.Models
{
    public class ChatModel
    {
        public string UserName;
        public string ChannelSid;
        public string ChannelMember;
        public List<ChannelModel> Channels;
        public List<UserResource> Users;
        public List<MessageResource> Messages;
    }
}
