﻿$(function () {
    // Get handle to the chat div
    var $chatWindow = $('#msg_history');

    // Our interface to the Chat service
    var chatClient;

    // A handle to the current chat channel
    var currentChannel;

    // The client username 
    var username = $('#username').val();

    // The channel sid
    var channelSid = $('#channelsid').val();

    // Helper function to print chat message to the chat window
    function printMessage(fromUser, message, time) {
        var $container = $('<div>');
        if (fromUser === username) {
            $container.addClass('outgoing_msg');            
        }
        else {
            $container.addClass('incoming_msg');
        }

        var $time = $('<span class="time_date"></span>').text(time.toLocaleTimeString());
        var $message = $('<p>').text(message);
        $container.append($time).append($message);
        $chatWindow.append($container);
        $chatWindow.scrollTop($chatWindow[0].scrollHeight);
    }

    // Alert the user they have been assigned a random username
    console.log('Logging in...');

    // Get an access token for the current user, passing a username (identity)
    // and a device ID - for browser-based apps, we'll always just use the
    // value "browser"
    $.getJSON('/token', {
        identity: username,
        channel: channelSid,
        device: 'browser'
    }, function (data) {
        // Initialize the Chat client
        Twilio.Chat.Client.create(data.token).then(client => {
            console.log('Created chat client');
            chatClient = client;
            chatClient.getSubscribedChannels().then(joinChannel()); 
        }).catch(error => {
            console.error(error);
        });
    });

    function joinChannel() {
        // Get the chat channel, which is where the messages are sent
        console.log('Attempting to join ' + channelSid + ' chat channel...');
 
        chatClient.getChannelBySid(channelSid)
            .then(function (channel) {
                currentChannel = channel;
                console.log('Found channel:');

                // Listen for new messages sent to the channel
                currentChannel.on('messageAdded', function (message) {
                    printMessage(message.author, message.body, message.dateUpdated);
                });
            }).catch(function () {
                console.log('Channel does not exist');
            });
    }

    // Send a new message to the channel
    function sendMessage() {
        if (currentChannel === undefined) {
            console.error('The Chat Service is not configured. Please check your dotnet secrets.', false);
            return;
        }
        currentChannel.sendMessage($input.val());
        $input.val('');
    }

    var $input = $('#chat-input');
    $input.on('keydown', function (e) {
        if (e.keyCode == 13) {
            sendMessage();
        }
    });

    var $msgButton = $('#chat_send_btn');
    $msgButton.on('click', function (e) {
        sendMessage();
    });

    $("#usersTable").on("click", "td", function () {
        var member = $(this).text();
        $.post("/Chat/Create",
            {
                username: username,
                member: member
            }).done(function (data) {
                window.location.reload();
        });
    });

    $("#deleteChannel").on("click", "td", function () {
        var member = $(this).text();
        $.post("/Chat/Create", { username: username, member: member });
    });
});
